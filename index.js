const searchBar = document.getElementById("search-bar");
const iconClick = document.getElementById("iconClicker");
const form = document.querySelector("form");
const weatherCardList = document.getElementById("weather-card-list");
const resultsContainer = document.getElementById("results-container");
const body = document.body;
let formValue;
const Madrid = 3117735,
  Rome = 3169070,
  Paris = 6455259,
  London = 2643743,
  Budapest = 3054638,
  Prague = 3067696,
  Brussels = 2800866,
  Amsterdam = 5107152,
  Barcelona = 3128760,
  Stockholm = 2673722,
  Lisbon = 6458923,
  SaintPetersburg = 536203,
  Milan = 3173435,
  Dubrovnik = 3201047,
  Athens = 4180386,
  Istanbul = 745044,
  Oslo = 3143244,
  Bucharest = 683506,
  Dublin = 4192205,
  Ibiza = 6356034;

/*PUT YOUR OWN KEY HERE - THIS MIGHT NOT WORK
SUBSCRIBE HERE: https://home.openweathermap.org/users/sign_up*/
const apiKey = "4d8fb5b93d4af21d66a2948710284366";

form.addEventListener("keyup", (e) => {
  formValue = e.target.value;
});

form.addEventListener("submit", (e) => {
  e.preventDefault();
  if (formValue) {
    fetchAPIAndRender(formValue);
    form.reset();
  }
});
iconClick.addEventListener("click", () => {
  if (formValue) {
    fetchAPIAndRender(formValue);
  }
});

// Madrid, Rome, Paris, London, Budapest, Prague, Brussels, Amsterdam, Sofia, Barcelona, Stockholm, Lisbon, SaintPetersburg, Milan, Barcelona, Dubrovnik, Athens, Istanbul, Oslo, Bucharest, Ibiza
let twentyCities = getTwentyCities();

async function getTwentyCities() {
  const APICallTwenty = await fetch(`https://api.openweathermap.org/data/2.5/group?id=${Madrid + "," + Rome + "," + Paris + "," + London + "," + Budapest + "," + Prague + "," + Brussels + "," + Amsterdam + "," + Barcelona + "," + Stockholm + "," + Lisbon + "," + SaintPetersburg + "," + Milan + "," + Dubrovnik + "," + Athens + "," + Istanbul + "," + Oslo + Bucharest + "," + Ibiza}&appid=${apiKey}&units=metric`);
  twentyCities = await APICallTwenty.json();
  return twentyCities;
}

const filterCities = (averageTempOfYourCity, fullList) => {
  return fullList.list.filter((obj) => obj.main.temp < averageTempOfYourCity);
};

async function fetchAPIAndRender(formValue) {
  const res = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${formValue}&appid=${apiKey}&units=metric`);
  const homeCity = await res.json();

  if (homeCity.cod !== 200) {
    homeCity.message ? alert(sentanceCase(homeCity.message)) : alert("API error");
  } else {
    const {
      main: { temp: averageTemp },
    } = homeCity || {};
    const colderCities = filterCities(averageTemp, twentyCities);

    if (colderCities.length === 0) {
      body.classList.add("cold");
      weatherCardList.innerHTML = "";

      const li = document.createElement("li");
      let markup = `
<div class="ui card">
  <div class="image">
    <img src="https://www.downloadclipart.net/medium/26591-snowman-images.png">
  </div>
  <div class="content">
    <a class="header">Unfortunately, you live in the coldest place!</a>
    <div class="meta">
      <span class="date">Temperature today: Very Cold</span><br/>
      <span class="date"></span><br/>
    </div>
  </div>
</div>
`;
      li.innerHTML = markup;
      weatherCardList.appendChild(li);
    } else {
      body.classList.remove("cold");
      weatherCardList.innerHTML = "";

      colderCities.forEach((obj) => {
        let {
          weather: [{ main: quickDescription = "🤷‍♂️", description: detailedDescription = "🤷‍♂️", icon }],
          main: { temp: averageTemp = "🤷‍♂️", temp_min: dailyMin = "🤷‍♂️", temp_max: dailyMax = "🤷‍♂️" },
          wind: { speed: windSpeed = "🤷‍♂️" },
          name: cityName = "",
          sys: { country = "" },
        } = obj;

        resultsContainer.setAttribute("class", "results-section");
        renderCard(detailedDescription, icon, averageTemp, cityName, country);
      });
    }
  }
}

function renderCard(detailedDescription, icon, averageTemp, cityName, country) {
  const li = document.createElement("li");
  let markup = `
<div class="ui card">
  <div class="image">
    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${icon}.svg">
  </div>
  <div class="content">
    <a class="header">${cityName}, ${country}</a>
    <div class="meta">
      <span class="date">Temperature today: ${averageTemp}°C</span><br/>
      <span class="date">Summary: ${sentanceCase(detailedDescription)}</span><br/>
    </div>
  </div>
</div>
`;
  li.innerHTML = markup;
  weatherCardList.appendChild(li);
}

function sentanceCase(string) {
  string = string[0].toUpperCase() + string.slice(1);
  return string;
}
